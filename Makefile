.PHONY: default docs clean

# Adapted from https://dev.to/eugenebabichenko/generating-pretty-version-strings-including-nightly-with-git-and-makefiles-48p3
DIRTY=$(shell git status --porcelain | head -1 | sed 's/.*/-dirty/g')
COMMIT=$(shell git rev-parse --short HEAD)
DATE=$(shell git log -1 --format=%cd --date=format:"%Y%m%d")
LAST_TAGGED_COMMIT=$(shell git rev-list --abbrev-commit --tags --max-count=1)
LAST_TAG=$(shell git describe --abbrev=0 --tags $(LAST_TAGGED_COMMIT) 2>/dev/null || true)
PATCH=$(patsubst %,-%-$(DATE),$(filter-out $(COMMIT),$(LAST_TAGGED_COMMIT)))
VERSION=$(LAST_TAG:v%=%)$(PATCH)$(DIRTY)

default: all

# List of documents to produce from markdown
DOCUMENTS=$(wildcard docs/%.mmd)
HTML_DOCS=$(patsubst docs/%.mmd,build/%.html,$(DOCUMENTS))
PDF_DOCS=$(patsubst docs/%.mmd,build/%.pdf,$(DOCUMENTS))

all: docs

docs: $(HTML_DOCS) $(PDF_DOCS)

build/%.html: docs/%.mmd
	sed 's/%VERSION%/$(VERSION)/g' $^ | multimarkdown -thtml > $@

build/%.pdf: docs/%.mmd
	sed 's/%VERSION%/$(VERSION)/g' $^ | multimarkdown -tlatex > build/$*.tex
	pdflatex -output-directory=build -interaction=batchmode build/$*.tex
	pdflatex -output-directory=build -interaction=batchmode build/$*.tex
	-rm $*.aux $*.toc $*.log build/$*.tex $*.out

clean:
	-rm $(HTML_DOCS) $(PDF_DOCS)

print-%:
	@echo '$*: $($*)'

# Project-level note: mmd files can have comments via ```{=comment} ... or {=todo}
