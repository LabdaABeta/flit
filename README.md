# Four-Letter Interleaved T(est/ask) Protocol

FLIT is a protocol for test reporting.


## License

This project is [Apache 2.0](http://www.apache.org/licenses/LICENSE-2.0) licensed.
